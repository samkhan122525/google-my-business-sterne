<?php declare(strict_types=1);

namespace Plugin\trigoda_gmb_stars;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
#use JTL\Link\LinkInterface;
use JTL\Services\JTL\AlertServiceInterface;
use JTL\Services\JTL\AlertService;

/**
 * Class Bootstrap
 * @package Plugin\trigoda_gmb_stars
 */
class Bootstrap extends Bootstrapper
{
    private $cache = null;
    private $cacheID = null;
    protected $alertCounter = 0;
    protected $checkResult = null;
    
    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher)
    {
        parent::boot($dispatcher);
        
        /* admin hook into plugin save options */
        $dispatcher->listen('shop.hook.' . \HOOK_PLUGIN_SAVE_OPTIONS, function (array $args) {
        	if ( $args['plugin']->getPluginID() !== 'trigoda_gmb_stars' ) {
        		return;
        	}
        	// check if API Key and Place ID are not empty
        	foreach( $args['options'] AS $option ) {
        		if( $option->valueID === 'trigoda_gmb_api_key' && empty( $option->value ) ) {
        			$this->dbg ( 'Du musst Deinen Google Places API Key eingeben, damit das Plugin funktioniert.', 0 );
        		}
        		else if( $option->valueID === 'trigoda_gmb_place_id' && empty( $option->value ) ) {
        			$this->dbg ( 'Du musst Deine Google PlaceID eingeben, damit das Plugin funktioniert.', 0 );
        		}
        	}
        	#$this->dbg ( 'Im Hook HOOK_PLUGIN_SAVE_OPTIONS '.$args['plugin']->getPluginID(), 1 );
        	#$this->dbg ( 'HasError: '. print_r( $args['hasError'], true), 2 );
        	#$this->dbg ( 'msg: '. print_r( $args['msg'], true), 2 );
        	#$this->dbg ( 'error: '. print_r( $args['error'], true), 2 );
        	#$this->dbg ( 'options: '. print_r( $args['options'], true), 2 );
        	// do a check in admin area, if plugin settings are correct, otherwise output admin alert
        	$this->checkResult = $this->checkSettings();

        });

        if (Shop::isFrontend() === false) {
            return;
        }
        
        $this->cache = Shop::Container()->getCache();
        $this->cacheID = 'trigoda_gmb_stars_'.$this->getPlugin()->getCache()->getID();
        if ( ( $GoogleApiResults = $this->cache->get($this->cacheID) ) === false ) {
            $GoogleApiResults = $this->requestGoogleApiResult();
            $caching_duration = $this->calculateCacheDuration();
            $this->cache->set($this->cacheID, $GoogleApiResults, [$this->getPlugin()->getCache()->getGroup()], $caching_duration);
        }
        
        /* add vote results to the box template */
        $dispatcher->listen('shop.hook.' . \HOOK_SMARTY_INC, function (array $args) use ($GoogleApiResults) {
            $args['smarty']->assign('trigoda_gmb_voteaverage', $GoogleApiResults[0])
				->assign('trigoda_gmb_votecount', $GoogleApiResults[1])
				->assign('trigoda_gmb_text_1', $this->getPlugin()->getLocalization()->getTranslation('trigoda_gmb_text_1'))
				->assign('trigoda_gmb_text_2', sprintf($this->getPlugin()->getLocalization()->getTranslation('trigoda_gmb_text_2') ?? '', $GoogleApiResults[0], $GoogleApiResults[1]))
				->assign('trigoda_gmb_text_3', $this->getPlugin()->getLocalization()->getTranslation('trigoda_gmb_text_3'))
				->assign('trigoda_gmb_ConfigVars', $this->getPlugin()->getConfig());
        });
        
        /* add vote results to the content without box */
        $dispatcher->listen('shop.hook.' . \HOOK_SMARTY_OUTPUTFILTER, function (array $args) {
        	// ACHTUNG: in diesem Hook funktioniert der AlertService nicht mehr
        	if ( $this->getPlugin()->getConfig()->getValue('trigoda_gmb_pqactive') === 'on' ) {
				$template = $this->getPlugin()->getPaths()->getFrontendPath() . 'boxes/' . 'trigoda_gmb_stars.tpl';
				$pq_element = $this->getPlugin()->getConfig()->getValue('trigoda_gmb_pqselector');
				$pq_method = $this->getPlugin()->getConfig()->getValue('trigoda_gmb_pqfunction');
				if ( count( pq($pq_element)->elements ) > 0 ) {
					pq($pq_element)->$pq_method($args['smarty']->fetch($template));
				}
        	}
        });
        
        $dispatcher->listen('shop.hook.' . \HOOK_LETZTERINCLUDE_CSS_JS, function (array $args) {
        	$choosenStyle = $this->getPlugin()->getConfig()->getValue('trigoda_gmb_box_template');
        	if ( $choosenStyle == 1  ) {
        		return;
        	}
        	foreach($args['cPluginCss_arr'] AS $k => $v) {
        		if ( strpos($v, 'trigoda_gmb_stars') >= 0) {
        			$args['cPluginCss_arr'][$k] = preg_replace('/trigoda_gmb_stars_(\d)\.css/', 'trigoda_gmb_stars_'.$choosenStyle.'.css', $v);
        		}
        	}
        });

    }

    /**
     * @inheritdoc
     */
    public function installed()
    {
        parent::installed();
    }

    /**
     * @inheritdoc
     */
    public function updated($oldVersion, $newVersion)
    {
        \error_log('updated from ' . $oldVersion . ' to ' . $newVersion);
    }

    /**
     * @inheritdoc
     */
    public function uninstalled(bool $deleteData = true)
    {
        parent::uninstalled($deleteData);
    }

    /**
     * @inheritdoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $plugin     = $this->getPlugin();
        $template = 'einleitung.tpl';
        return $smarty->assign('ShopURL', $this->getPlugin()->getPaths()->getShopURL())
        			->fetch($this->getPlugin()->getPaths()->getAdminPath() . '/templates/' . $template);
    }

    /**
     * @inheritdoc
     */
    public function requestGoogleApiResult(): array
    {
		
		$json_url = sprintf ( 'https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&fields=user_ratings_total,rating&key=%s', 
								$this->getPlugin()->getConfig()->getValue('trigoda_gmb_place_id'), 
								$this->getPlugin()->getConfig()->getValue('trigoda_gmb_api_key')
							);
		$json_content = file_get_contents ( $json_url );

		if ( isset ( $json_content ) AND $json_content !== false ) {
			$json_data = json_decode ( $json_content, true );
			if ( isset ( $json_data ) ) {
				if ( isset ( $json_data['result'] ) ) {
					$rating = $json_data['result']['rating'] ?? 0;
					$user_ratings_total = $json_data['result']['user_ratings_total'] ?? 0;
					return [ $rating, $user_ratings_total ];
				} else {
					$this->dbg ( __METHOD__.' json_data error: '.$json_data['error_message'], 0 );
				}
			} else {
				$this->dbg ( __METHOD__.' json_data invalid', 0 );
			}
		}
		return [ 0, 0 ];
    }

    /**
     * @inheritdoc
     */
    public function checkSettings(): bool
    {
    	$e = 0;
    	if ( $this->getPlugin()->getConfig()->getValue('trigoda_gmb_pqactive') === 'on' ) {
			$pq_method = $this->getPlugin()->getConfig()->getValue('trigoda_gmb_pqfunction');
			$pq_allowed_methods = ['append', 'prepend', 'after', 'before', 'replaceWith'];
			if ( ! in_array( $pq_method, $pq_allowed_methods ) ) {
				$this->dbg ( 'Die gespeicherte PHPQuery-Funktion <i>'.htmlentities($pq_method).'</i> ist nicht zugelassen', 0 );
				$e++;
			}
		}
		#$GoogleApiResults = $this->requestGoogleApiResult();
        #if ( $GoogleApiResults[0] === 0 && $GoogleApiResults[1] === 0  ) {
        #	$this->dbg ( 'Die Google API Abfrage Eergebnisse sind fehlerhaft, prüfe die Konfiguration', 0 );
		#	$e++;
        #}
    	if ( $e > 0 ) {
    		return false;
    	}
        return true;
    }

    /**
     * @inheritdoc
     */
    public function dbg( $text, $level ): void
    {
    	$alertService = Shop::Container()->getAlertService();
    	if( $level === 0 ) {
    		$alertService->addError($text, 'TGMBPluginDebugError_'.$this->alertCounter, ['dismissable' => true, 'showInAlertListTemplate' => true]);
    	}
    	else if( $level === 1 ) {
    		$alertService->addWarning($text, 'TGMBPluginDebugWarning_'.$this->alertCounter, ['dismissable' => true, 'showInAlertListTemplate' => true]);
    	}
    	else if( $level === 2 ) {
    		$alertService->addInfo($text, 'TGMBPluginDebugInfo_'.$this->alertCounter, ['dismissable' => true, 'showInAlertListTemplate' => true]);
    	}
    	else if( $level === 3 ) {
    		$alertService->addSuccess($text, 'TGMBPluginDebugSuccess_'.$this->alertCounter, ['dismissable' => true, 'showInAlertListTemplate' => true]);
    	}
    	else if( $level === 4 ) {
    		$alertService->addDanger($text, 'TGMBPluginDebugDanger_'.$this->alertCounter, ['dismissable' => true, 'showInAlertListTemplate' => true]);
    	}
    	else if( $level === 5 ) {
    		$alertService->addNotice($text, 'TGMBPluginDebugNotice_'.$this->alertCounter, ['dismissable' => true, 'showInAlertListTemplate' => true]);
    	}
    	$this->alertCounter++;
    	#Shop::dbg($text . $level);
    	#print $text . $level;
    }

    /**
     * @inheritdoc
     */
    public function calculateCacheDuration(): int
    {
    	$setting = $this->getPlugin()->getConfig()->getValue('trigoda_gmb_box_call_frequency');
    	$ret = 1800;
    	switch ($setting) {
    		case 1: $ret = 3600; break;
    		case 2: $ret = 3600 * 24; break;
    		case 3: $ret = 3600 * 24 * 7; break;
    		case 4: $ret = 3600 * 24 * 30; break;
    	}
        return $ret;
    }
    
}
