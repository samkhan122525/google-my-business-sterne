# Trigoda Google My Business Sterne

![Trigoda Google My Business Stars Logo](preview.png)

Mit diesem Plugin kannst Du die Sterne Bewertungen Deines Google My Business Profils (Unternehmensprofil) auf Deinem Shop anzeigen lassen.

Das Plugin wird Dir von **[&raquo;Trigoda UG&laquo;](https://trigoda.de)** zur Verfügung gestellt, es ist Open-Source und kostenlos. Kein Ioncube oder so was ;=)

### Voraussetzungen

- Du benötigst einen Google Api Key für die Places Api. **[&raquo;Api Key erstellen&laquo;](https://developers.google.com/maps/documentation/places/web-service/get-api-key?hl=de)**
- Du benötigst die Place ID von Ihrem Google My Business Unternehmensprofil **[&raquo;Place ID finden&laquo;](https://developers.google.com/maps/documentation/places/web-service/place-id?hl=de)**
- Und du benötigst natürlich vorhandene Bewertungen

### Die Einstellungen

Im folgenden erläutern wir Dir alle Plugin Einstellungen im Detail.

- **Google Einstellungen**
	- Api Key: Hier bitte Deinen Google Place API Key eingeben (ohne diesen funktioniert das Plugin nicht)
	- Google Place ID: Bitte gebe hier die Google Place ID Deines Unternehmens ein (ohne diesen funktioniert das Plugin nicht)
	- Abruf Frequenz: Die Mögliechkeit, die Abruffrequenz einzustellen, schützt Euch vor hohen API Kosten und macht das ganze Plugin performanter. Empfohlene Einstellung: täglich
- **Anzeige Einstellungen**
	- Box Template: Das Design Template auswählen, wir haben derzeit 3 verschiedene Vorlagen für die Sterne Bewertungen zur Auswahl erstellt
	- Link zu Google: Wenn das Häkchen hier gesetzt ist, kann der Nutzer auf das Sterne Widget klicken, um selber eine Bewertung zu verfassen und Dein Unternehmensprofil anzusehen
	- Stern Farbe aktiv: Die Farbe für die vergebenen Sterne (im Vordergrund)
	- Stern Farbe passiv: Die Farbe für nicht vergebene Sterne im Hintergrund (damit immer fünf Sterne zu sehen sind)
	- Weitere Style Anpassungen lassen sich natürlich per eigenen CSS umsetzen
- **Position auf der Seite**
	- Du hast zwei Möglichkeiten, die Sterne Ausgabe in Deine Seite einzubinden. 
		1. Per Box, welche Du in der Administration unter **Darstellung -> Standardelemente -> Footer / Boxen** in deine Seite/n an der gewünschten Position einbinden kannst. Du findest die Plugin Box in der Auswahl bei "Neue Box" ganz unten bei Plugins, sie heisst "Google My Business Sterne"
		2. Per PHPQuery, siehe die folgenden Punkte
	- PHPQuery Einbindung aktivieren: Setze das Häkchen, um die Einbindung via PHPQuery zu aktivieren
	- PHPQuery-Selektor: Auswählen, an welchem Element (per HTML Attribut ID bzw. PHPQuery) die Google Bewertungen angefügt werden sollen. Das Element muss vorhanden sein
	- PHPQuery-Funktion: Auswählen, wie der Inhalt an das zuvor per Selektor gewählte Element eingefügt werden soll

### Sprachvariablen anpassen

Damit Du Kontrolle über die ausgegebenen Texte und Übersetzungen hast, haben wir in dem Plugin einige Sprachvariablen angelegt, welche Du über den Button Sprachvariablen in der Plugin Einleitung oder im Plugin-Manager bearbeiten kannst:

- Text 1 (trigoda_gmb_text_1): Erste Textzeile im Plugin, etwas wie "Unsere Google Bewertung:" 
- Text 2 (trigoda_gmb_text_2): Zweite Textzeile im Plugin, enthält die Bewertungswerte, daher sind dort 2 Platzhalter für den Durchschnitt der Bewertungen und die Anzahl der Bewertungen
- Text 3 (trigoda_gmb_text_3): Hiermit kannst Du den Text auf dem Button mit dem Link zum Bewertung verfassen anpassen (erscheint nur, wenn "Link zu Google" aktiviert ist)

Die Sprachvariablen sind derzeit in Englisch und Deutsch vor definiert.

### Plugin anpassen

Unser Plugin ist Open Sorce, Du kannst es nach belieben Deinen Wünschen anpassen und die angepasste Version in Deinem JTL-Shop installieren. Den Code dafür kannst Du von unserem **[&raquo;Gitlab Plugin Repository&laquo;](https://gitlab.com/trigoda-team/JTL-Shop-Plugins/google-my-business-sterne)** beziehen.

### Mitwirken

Du möchtest das Plugin verbessern oder anpassen? Du kannst gerne das **[&raquo;Plugin Repository&laquo;](https://gitlab.com/trigoda-team/JTL-Shop-Plugins/google-my-business-sterne)** forken und nach belieben anpassen. Wenn Du Deine Änderungen an andere weitergeben möchtest, kannst Du dann gerne Merge Requests vorschlagen. Nutze gerne die Gitlab Issues, um Fehler zu melden oder Verbesserungsvorschläge zu machen.

### Change Log

1.0.0 - Initiale Version vom 30.10.2023

### Support

Wenn Du Hilfe bei der Nutzung des Plugins benötigst, stehen wir Dir gerne zur Verfügung. Bedenke aber bitte, dass wir dieses Plugin kostenlos zur Verfügung stellen! Daher behalten wir uns vor, individuelle Anpassungen oder Wünsche nur gegen Bezahlung umzusetzen.
